### Paper:乳腺超声双模态数据的协同约束网络
`A cooperative suppression network for bimodal data in breast cancer classification`

现阶段大部分研究者仅采用B型超声图像作为实验数据.
但是B型超声自身的局限性导致分类效果难以提升。
针对该问题，提出了一种综合利用B型超声和超声造影视频来提高分类精度的网络模型.

* 杨子奇, 龚勋, 朱丹, 郭颖, "乳腺超声双模态数据的协同约束网络," 中国图象图形学报, vol. 25, no. 10, 2218-2228, 2020, doi: 10.11834/jig.200246. *

### 视频讲解
`同学帮/视觉系`：[https://space.bilibili.com/202603446](https://space.bilibili.com/202603446)